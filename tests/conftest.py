import pytest


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "gmsh_model(name): pass model name to gmsh"
    )

# def pytest_addoption(parser):
#     """
#     We add the --pdf and --type options to be able to
#     specify the PDF filename and type.
#     """
#     parser.addoption("--pdf", action="store", default=None, help="filename")
#     parser.addoption(
#         "--type", action="store", default=None, help="type of file"
#     )


# @pytest.fixture(scope="session")
# def pdf(request):
#     """
#     Make sure that the PDF filename is available.
#     """
#     return request.config.getoption("--pdf")
