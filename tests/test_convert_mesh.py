from pathlib import Path
import pytest
import gmsh

from tools.convert_msh import convert_msh


# @pytest.fixture
# def config():
#     return yaml.load(open("tests/config.yml"), Loader=yaml.FullLoader)


@pytest.fixture(scope="session")
def gmsh_session():
    gmsh.initialize()

    yield
    gmsh.finalize()


@pytest.fixture(scope="function")
def gmsh_convert(gmsh_session, tmp_path, request):
    marker = request.node.get_closest_marker("gmsh_model")
    assert marker

    path = Path("tests", "models", f"{marker.args[0]}.geo")
    assert path.exists()

    gmsh.open(str(path))
    gmsh.model.mesh.generate(2)
    path = Path(tmp_path, f"{marker.args[0]}.msh")
    gmsh.write(str(path))
    assert path.exists()

    return path


@pytest.mark.gmsh_model("PEC_pipe")
def test_convert_PEC_pipe(gmsh_convert):
    assert gmsh_convert.exists()
    convert_msh(gmsh_convert, gmsh_convert.with_suffix(".xdmf"))
    assert gmsh_convert.with_suffix(".xdmf").exists()

@pytest.mark.gmsh_model("steel_pipe")
def test_convert_steel_pipe(gmsh_convert):
    assert gmsh_convert.exists()
    convert_msh(gmsh_convert, gmsh_convert.with_suffix(".xdmf"))
    assert gmsh_convert.with_suffix(".xdmf").exists()

@pytest.mark.gmsh_model("ferrite_ring")
def test_convert_ferrite_ring(gmsh_convert):
    assert gmsh_convert.exists()
    convert_msh(gmsh_convert, gmsh_convert.with_suffix(".xdmf"))
    assert gmsh_convert.with_suffix(".xdmf").exists()

@pytest.mark.gmsh_model("collimator")
def test_convert_collimator(gmsh_convert):
    assert gmsh_convert.exists()
    convert_msh(gmsh_convert, gmsh_convert.with_suffix(".xdmf"))
    assert gmsh_convert.with_suffix(".xdmf").exists()
