#!/usr/bin/env bash

set -e

SOURCE_DIR=$(realpath $(dirname $0))
BUILD_DIR=$SOURCE_DIR/build-dir

BASIX_COMMIT=58c620b1391fd4b1eaa3c3840f8452d48f69619f
DOLFIN_COMMIT=61e7a9918fadb2a52ca2932452e7299feb421cc5
UFL_COMMIT=af99cad50cde6d041d856556cba130ef58b5824e
FFCX_COMMIT=9a0348b432f560f57e91c99bb1d48dff616cf88a

git clone https://github.com/FEniCS/basix.git $SOURCE_DIR/basix
git -C $SOURCE_DIR/basix checkout $BASIX_COMMIT

git clone https://github.com/FEniCS/dolfinx.git $SOURCE_DIR/dolfinx
git -C $SOURCE_DIR/dolfinx checkout $DOLFIN_COMMIT

git clone https://github.com/FEniCS/ufl.git $SOURCE_DIR/ufl
git -C $SOURCE_DIR/ufl checkout $UFL_COMMIT

git clone https://github.com/FEniCS/ffcx.git $SOURCE_DIR/ffcx
git -C $SOURCE_DIR/ffcx checkout $FFCX_COMMIT

scalar_type=complex
LIB_PATH=$SOURCE_DIR/lib

mkdir -p $LIB_PATH

CMAKE_PREFIX_PATH="$CMAKE_PREFIX_PATH:$LIB_PATH/lib/cmake/:$LIB_PATH/share/cmake/"
PETSC_DIR="/usr/lib/petscdir/petsc-$scalar_type"
SLEPC_DIR="/usr/lib/slepcdir/slepc-$scalar_type"
PETSC_ARCH="linux-gnu-$scalar_type-64"
PYTHONPATH="$PETSC_DIR/lib/python3/dist-packages:$SLEPC_DIR/lib/python3/dist-packages"
PKG_CONFIG_PATH="$PETSC_DIR/lib/pkgconfig:$PKG_CONFIG_PATH"
LD_LIBRARY_PATH="$PETSC_DIR/lib:$LD_LIBRARY_PATH"

# Basix
rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$LIB_PATH \
      -B $BUILD_DIR -S $SOURCE_DIR/basix/cpp
cmake --build $BUILD_DIR -j$(nproc)
cmake --install $BUILD_DIR

pip3 install --user $SOURCE_DIR/basix/python
# UFL
pip3 install --user $SOURCE_DIR/ufl
# FFCX
pip3 install --user $SOURCE_DIR/ffcx

    # DolfinX
rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$LIB_PATH \
      -B $BUILD_DIR -S $SOURCE_DIR/dolfinx/cpp
cmake --build $BUILD_DIR -j$(nproc)
cmake --install $BUILD_DIR
source $LIB_PATH/lib/dolfinx/dolfinx.conf

# MPI
pip3 install --user mpi4py
# PETSc
pip3 install --user petsc4py
# SLEPc
pip3 install --user slepc4py

if [[ $scalar_type == "complex" ]]; then
    PETSC_USE_COMPLEX=1 pip3 install --user $SOURCE_DIR/dolfinx/python
else
    pip3 install --user $SOURCE_DIR/dolfinx/python
fi
